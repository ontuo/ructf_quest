/*
* ruCTF.c
*
* Created: 04.04.2019 0:33:15
* Author : tuo
*/

#define F_CPU 16000000L

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


#define RANG	4

char port_state[RANG-1]= {0xEF,0xDF,0xBF};
char input_state[RANG]={0x01,0x02,0x04,0x08};
char symbol[RANG-1][RANG]={{'1','4','7','*'},
						 {'2','5','8','0'},
						 {'3','6','9','#'}};
char PASS[RANG] = {'8', '1', '0', '9'};	
int  DIODES[5] = {0x02, 0x06, 0x0E, 0x1E, 0x3E};
char answer[RANG] = {0,0,0,0};
int num_queue = 0;
int flag = 0;
int check_password();

ISR(TIMER1_OVF_vect){
	for (int i = 0; i < RANG-1; i++){
		PORTD = port_state[i];
		for (int j = 0; j < RANG; j++){

			if((PIND & input_state[j]) == 0){
				while((PIND & input_state[j]) != input_state[j]){};
				_delay_ms(10);
				if (num_queue < 4){
					flag = 1;
					answer[num_queue] = symbol[i][j];
					num_queue ++;	
				}
			}
		}
	}
}

int check_password(){
	for(int i = 0; i < RANG; i++){
		if (PASS[i] != answer[i]){
			return 0;
		}
	}
	return 1;
}

int main(void)
{

	DDRD=0xF0;
	PORTD=0xFF;
	
	DDRC = 0x3E;
	PORTC = 0x00;
	
	TCCR1B |= 1<<CS10;
	TIMSK1 |= 1<<TOIE1;
	
	sei();
	
	while (1)
	{
		if(num_queue < 4 && flag == 1){
			PORTC = DIODES[num_queue - 1];
		}
		if (num_queue > 3){
			if(check_password() == 0){
				for(int i = 0; i < RANG; i++){
					PORTC = 0x00;
					_delay_ms(10);
					PORTC = 0x1E;
					_delay_ms(10);
				}
			}
			else{
				PORTC = 0xFF;
				_delay_ms(50);
			}
			for (int m = 0; m< RANG; m++){
				answer[m] = 0;
			}
			num_queue = 0;
			PORTC = 0x00;
			flag = 0;
		}
	}
}