#ifndef USART_H
#define USART_H

#define F_CPU 16000000L
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void usart_transmit(unsigned char data);
void usart_init(unsigned long speed);
void usart_transmit(unsigned char data);
void usart_str_transmit(char* data, int lentgh);
void usart_int_transmit(int data);
void usart_char_transmit(char data);

#endif