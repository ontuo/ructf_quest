#include "usart.h"

void usart_init(unsigned long ubrr){
	if (ubrr == 115200) ubrr = 16;
	if (ubrr == 9600) ubrr = 207;
	if (ubrr == 19200) ubrr = 103;
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	
	UCSR0B=(1<<RXEN0)|( 1<<TXEN0); // in & out
	UCSR0B |= (1<<RXCIE0); // en interapt in input
	UCSR0A |= (1<<U2X0);
	UCSR0C |= (1<<UCSZ01)|(1<<UCSZ00);
}

void usart_transmit(unsigned char data){
	while( !(UCSR0A &(1<<UDRE0)) );
	UDR0 = data;
}

unsigned char usart_receive(){
	while (!(UCSR0A & (1 << RXC0)));
	return UDR0;
}

void usart_char_transmit(char data){
	usart_transmit(data);
	usart_transmit(0x0d);
	usart_transmit(0x0a);
}

void usart_str_transmit(char* data, int lentgh){
	for(int i = 0; i< lentgh; i++ ){
		usart_transmit(data[i]);
	}
	usart_transmit(0x0d);
	usart_transmit(0x0a);
}

void usart_int_transmit(int data){
	usart_transmit((char)data);
	usart_transmit(0x0d);
	usart_transmit(0x0a);
}